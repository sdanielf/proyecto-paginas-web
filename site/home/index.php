﻿<!DOCTYPE html>
<html lang="es">
  <head>
<?php
$contacto=false;
$galeria=false;
$titulo="";
include '../../base/head.php';
?>
  </head>
  <body>
    <header>
<?php
$current="index";
include '../../base/navbar.php';
?>
    </header>

    <section>
      <div id="parallax1" data-parallax="scroll" data-image-src="../../static/img/codigo.jpg">
        <h1>Queremos una internet más segura</h1>
      </div>

      <div class="container">
        <h2 class="page-header">Usuarios</h2>
        <p>
          Brindamos consejos para protegerse de amenazas como virus, malware y
          fraude en la red.
        </p>
        <h2 class="page-header">Desarrolladores</h2>
        <p>
          La seguridad es importante aplicarla al ciclo de vida del software para con el
          fin de tener calidad en los productos desarrollados y cumplir con los tres
          objetivos de la seguridad que son la integridad, confidencialidad y disponibilidad
          ya que en esta época la tecnología va creciendo donde y encontramos que la información
          que necesitan las personas ya es accesible desde internet.
        </p>
      </div>

      <div id="parallax2" data-parallax="scroll" data-image-src="../../static/img/seguridad.jpg">
      </div>

      <div class="container">
        <h1 class="page-header">Te ayudamos a asegurar tus sistemas</h1>
        <h3 style="padding: 10px;">
            <span class="label label-primary">Nuevo</span>
          <a href="password.html">
            Conoce nuestra herramienta para probar contrese&ntilde;as
          </a>
        </h3>
        <p>
          La seguridad web es uno de los caballos de batalla de cualquier empresa.
          Continuamente, muchas personas intentan acceder a los ordenadores ajenos
          para obtener los datos que poseemos. Ese acceso no autorizado a una red
          informática o a los equipos que engloba puede ocasionar graves problemas
          para la empresa afectada. La pérdida de datos, el robo de información
          sensible y confidencial o la divulgación de los datos de nuestros clientes
          pueden ocasionar graves pérdidas, tanto económicas como de credibilidad.
        </p>
        <h2 class="page-header">Administradores</h2>
        <p>Un buen sistema de seguridad:</p>
        <ul>
          <li>Evita ataques informáticos.</li>
          <li>Asegura los datos, evita su robo o la pérdida de los mismos.</li>
          <li>Evita la pérdida de las configuraciones.</li>
          <li>Minimiza el spam que se recibe.</li>
          <li>Evita la suplantación de identidad.</li>
          <li>Mejora la experiencia de tus usuarios.</li>
        </ul>
      </div>
    </section>

    <?php include '../../base/footer.php' ?>
    <script src="../../static/js/parallax.js"></script>
  </body>
</html>
