<!DOCTYPE html>
<html lang="es">
  <head>
<?php
$contacto=false;
$galeria=false;
$titulo="Consejos para usuarios";
include '../../base/head.php';
?>
  </head>

  <body>
    <header>
<?php
$current="consejos";
include '../../base/navbar.php';
?>
      <div class="jumbotron">
        <div class="container">
          <h1>Seguridad informática para usuarios</h1>
          <p>
            Este apartado de Seguridad Informática fue creado para que los
            usuarios comunes y corrientes se informen sobre que pueden hacer
            para mejorar la seguridad de sus dispositivos informáticos.
          </p>
        </div>
      </div>
    </header>

    <section class="container">
      <div class="row">
        <div class="col-md-4">
          <h2>Mantener los programas actualizados</h2>
          <p>
            Hay malware que se aprovecha de las vulnerabilidades en programas
            famosos para colarse en tu PC sin que el antivirus pueda actuar.
            Para evitar sorpresas, debes mantener actualizados tus programas.
          </p>
        </div>
        <div class="col-md-4">
          <h2>En redes públicas, navega con cifrado</h2>
          <p>
            En las redes WiFi públicas, tus datos pueden ser interceptados de
            muchas maneras. Navegar desde ellas sin protección es una
            imprudencia que se paga muy cara. Para defenderte, navega siempre
            con el protocolo HTTPS activado.
          </p>
       </div>
        <div class="col-md-4">
          <h2>Crea usuarios y contraseñas distintos</h2>
          <p>
            Casi cada día aparecen noticias sobre contraseñas robadas en servicios
            importantes. El riesgo de que entren en tus cuentas una vez atacado un
            servicio es enorme. Crea contraseñas distintas y seguras para todos tus
            servicios, y usa nombres de usuario diferentes cuando se te dé esa opción.
          </p>
        </div>
        <div class="col-md-4">
          <h2>Cambia tus contraseñas a menudo</h2>
          <p>
            Las contraseñas envejecen. Y si las vulnera un intruso discreto,
            puede que tardes mucho en saber si alguien ha accedido a tus
            archivos y mensajes. Por muy fuertes que sean tus contraseñas,
            cámbialas periódicamente.
          </p>
        </div>
        <div class="col-md-4">
          <h2>Comprueba las apps autorizadas</h2>
          <p>
            "¿Puede la aplicación X leer tus datos de Facebook y Google?".
            Cuando autorizas una app maligna, el desastre está servido: spam
            enviado en tu nombre, robo de datos, etc. Para prevenir esto,
            controla las apps autorizadas de Google, Facebook, Twitter y otros
            sitios.
          </p>
        </div>
        <div class="col-md-4">
          <h2>Protege tu red WiFi frente a intrusos</h2>
          <p>
            Una red WiFi abierta es un gesto solidario... y peligroso. Un
            visitante mal intencionado puede intentar acceder a los datos de tu
            ordenador. Y entonces hablamos de intrusos. Revisar la seguridad de
            tu red WiFi es la mejor manera de evitar sorpresas desagradables.
          </p>
        </div>
        <div class="col-md-4">
          <h2>Controla la privacidad de tus redes</h2>
          <p>
            En tus perfiles de Facebook y Google hay un montón de información
            personal que puede usarse en tu contra (por ejemplo, para adivinar
            contraseñas). Rechaza solicitudes de amistad sospechosas y
            configura bien la privacidad de Facebook y otras redes sociales. Es
            una cuestión de privacidad fundamental.
          </p>
        </div>
        <div class="col-md-4">
          <h2>Crea usuarios para cada persona</h2>
          <p>
            He visto un montón de ordenadores con una sola cuenta para toda
            la familia. O con varias cuentas, pero desprotegidas. Es un camino
            seguro hacia el desastre. Si más de una persona va a usar un PC,
            crea diferentes cuentas, cada una protegida por una contraseña
            fuerte u otro sistema de identificación. ¡Y por favor, bloquea el
            PC!
          </p>
        </div>
        <div class="col-md-4">
          <h2>Desconfía de los archivos que te envían</h2>
          <p>
            Uno de los virus más dañinos de los últimos tiempos se propagó
            a través de Skype: un amigo enviaba un archivo y la gente, al
            confiar en su origen, lo abría. Estés donde estés, no abras un
            archivo misterioso por ninguna razón, ni siquiera si te lo envía
            un amigo. Pregúntale antes qué es.
          </p>
        </div>
      </div>
    </section>

    <?php include '../../base/footer.php'; ?>
  </body>
</html>
