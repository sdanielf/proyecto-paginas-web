<!DOCTYPE html>
<html lang="es">
  <head>
<?php
$contacto=false;
$galeria=false;
$titulo="Bitácora";
include '../../base/head.php';
?>
  </head>
  <body>
    <header>
<?php
$current="bitacora";
include '../../base/navbar.php';
?>
    </header>
    <section class="container">
      <h1 class="page-header">
        <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
        Bitácora
      </h1>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Fecha de actualizado</th>
            <th>Asunto</th>
          </tr>
        </thead>
        <tbody>
<?php

$cambios=array(
  array("Lucas Di Landro", "2015-10-14", "Se optimizan las imágenes de la galería"),
  array("Daniel Francis", "2015-10-13", "Se adopta PHP en la construcción del sitio"),
  array("Daniel Francis", "2015-10-8", "Se agrega la herramienta para verificar contraseñas"),
  array("Gonzalo Gorostiza", "2015-10-1", "Se modifica el esquema se colores"),
  array("Daniel Francis", "2015-09-16", "Se añade contenido a la página de inicio"),
  array("Lucas Di Landro", "2015-09-13", "Se crea la sección de consejos para administradores y desarrolladores"),
  array("Gonzalo Gorostiza", "2015-09-10", "Se crea la sección de consejos para usuarios"),
  array("Gonzalo Gorostiza", "2015-09-08", "Se crea Barra de Navegación"),
  array("Daniel Francis", "2015-09-05", "Se crean los Parallax"),
  array("Lucas Di Landro", "2015-09-05", "Se crea la navegación según el árbol")
);

for ($i=0; $i < count($cambios); $i++) {
  $fila=$cambios[$i];
?>
          <tr>
            <td><?= $fila[0] ?></td>
            <td><?= $fila[1] ?></td>
            <td><?= $fila[2] ?></td>
          </tr>
<?php 
}
?>
        </tbody>
      </table>
    </section>

    <?php include '../../base/footer.php'; ?>
  </body>
</html>
