<!DOCTYPE html>
<html lang="es">
  <head>
<?php
$contacto=false;
$galeria=false;
$titulo="Consejos para desarrolladores y administradores";
include '../../base/head.php';
?>
  </head>

  <body>
    <header>
<?php
$current="consejos";
include '../../base/navbar.php';
?>

      <div class="jumbotron">
        <div class="container">
          <h1>Seguridad informática para desarrolladores y administradores</h1>
          <p>Este apartado de Seguridad Informática fue creado para los desarrolladores y administradores,
          para garantizar que, con los consejos a dar a continuacion, programar sea mas seguro que antes.</p>
        </div>
      </div>
    </header>

    <section class="container">
      <div class="row">
        <div class="col-md-4">
          <h2>Prueba rigurosamente las entradas de datos</h2>
          <p>Valida el tamaño y estructura de los datos entrantes consume tiempo, pero es la única forma de dar seguridad informática en tu código.</p>
        </div>
        <div class="col-md-4">
          <h2>Almacena sólo los datos que necesitas, y no más</h2>
          <p>Si vas a pedir la dirección de correo física, pregúntate si tu sistema alguna vez enviará correspondencia. Almacena sólo lo que realmente necesitaras, así te ahorraras almacenar datos cuya seguridad informática pueda verse comprometida.</p>
       </div>
        <div class="col-md-4">
          <h2>Evita depender de contraseñas más de lo necesario</h2>
          <p>Existen alternativas distintas a las contraseñas en la seguridad informática, tales como: Autenticación de factor-N, llaves criptográficas que bloquean el hardware y mantener un registro de direcciones IP validas para negar el acceso o solicitar información adicional.</p>
        </div>
        <div class="col-md-4">
          <h2>Construye muros</h2>
          <p>Siempre es recomendable segregar las funcionalidades más sensitivas en otro sistema, o agregar barreras de seguridad adicionales, por ejemplo una segunda clave para operaciones sensibles, o enviar contraseñas "Single Sign On" vía mensaje de texto o correo electrónico.</p>
        </div>
        <div class="col-md-4">
          <h2>Usa librerías probadas por la industria</h2>
          <p>La encriptación es difícil de implementar en la seguridad informática, no intentes inventar la rueda nuevamente, utiliza librerías disponibles que ya han sido probadas por la comunidad y sus errores son conocidos.</p>
        </div>
        <div class="col-md-4">
          <h2>Usa herramientas de análisis de código</h2>
          <p>Las herramientas de análisis de código y las herramientas de pruebas automatizadas, pueden encontrar fallas que se cometen cuando los programadores están cansados y no piensan con claridad. Muchos de estos errores son simples pero fatales.</p>
        </div>
        <div class="col-md-4">
          <h2>Establece límites a los privilegios de acceso</h2>
          <p>Un buen principio es darle al código y a los usuarios solamente los mínimos permisos que necesitan para ejecutar su función. Dar amplios permisos conduce inevitablemente a brechas en la seguridad informática.</p>
        </div>
        <div class="col-md-4">
          <h2>Analiza las amenazas</h2>
          <p>Invierte tiempo en pensar que datos almacenas, quien podría quererlos y como podrían serle útiles a un delincuente.</p>
        </div>
        <div class="col-md-4">
          <h2>Mantente actualizado con información de las más recientes amenazas</h2>
          <p>Busca constantemente artículos, noticias y líderes de la industria.</p>
        </div>
      </div>
    </section>

    <?php include '../../base/footer.php'; ?>
  </body>
</html>
