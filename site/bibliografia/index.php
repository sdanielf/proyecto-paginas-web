<!DOCTYPE html>
<html lang="es">
  <head>
<?php
$contacto=false;
$galeria=false;
$titulo="Bibliografía";
include '../../base/head.php';
?>
  </head>

  <body>
    <header>
<?php
$current="bibliografia";
include '../../base/navbar.php';
?>
    </header>

    <section class="container">
      <h1 class="page-header">
        <span class="glyphicon glyphicon-book" aria-hidden="true"></span>
        Bibliografía
      </h1>
      <h3>Articulos Softonic</h3>
      <p><a href="http://articulos.softonic.com/10-consejos-de-seguridad-informatica-imprescindibles">
        http://articulos.softonic.com/10-consejos-de-seguridad-informatica-imprescindibles</a></p>
      <h3>Open Directory Project (dmoz)</h3>
      <p><a href="https://www.dmoz.org/World/Espa%C3%B1ol/Inform%C3%A1tica/Seguridad/">
        https://www.dmoz.org/World/Espa%C3%B1ol/Inform%C3%A1tica/Seguridad/</a></p>
      <h3>PMO Informática</h3>
      <p><a href="http://www.pmoinformatica.com/2014/01/consejos-seguridad-informatica.html">
        http://www.pmoinformatica.com/2014/01/consejos-seguridad-informatica.html</a></p>
      <h3>Wikipedia</h3>
      <p><a href="https://es.wikipedia.org/wiki/Seguridad_inform%C3%A1tica">
        https://es.wikipedia.org/wiki/Seguridad_inform%C3%A1tica</a></p>
    </section>

    <?php include '../../base/footer.php'; ?>
  </body>
</html>
