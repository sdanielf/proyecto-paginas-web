<!DOCTYPE html>
<html lang="es">
  <head>
<?php
$contacto=true;
$galeria=false;
$titulo="Contacto";
include '../../base/head.php';
?>
  </head>

  <body>
    <header>
<?php
$current="contacto";
include '../../base/navbar.php';
?>
    </header>

    <section class="container">

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if ($_POST['g-recaptcha-response'] != '') {
    $from=$_POST['correo'];
    $nombre=$_POST['nombre'];
    $apellido=$_POST['apellido'];
    $asunto=$_POST['asunto'];
    $mensaje=$_POST['comentarios'];
    $body="De: $nombre $apellido <$from>\nAsunto: $asunto\nMensaje: $mensaje";
    mail(
      "Daniel Francis <santiago.danielfrancis@gmail.com>,".
      "Lucas Di Landro <lucasdilandro@gmail.com>,".
      "Gonzalo Gorostiza <isebgonzalogorostiza@gmail.com>",
      "[ESI Seguridad Informática] $asunto", $body);
?>
      <h1>¡Gracias por contactarnos!</h1>
      <h2>Responderemos tu mensaje lo más pronto posible</h2>
      <h3><a href="../home">Volver al inicio</a></h3>

<?php } else {?>
      <h1>Ha ocurrido un error</h1>
      <h2>El formulario no se ha enviado.</h2>
      <p>Intenta enviarlo nuevamente y asegúrate de verificar el captcha.</p>
      <h3><a href="../home">Volver al inicio</a></h3>
<?php
  }
} else {?>
      <h1 class="page-header">
        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
        Contactarnos
      </h1>
      <form class="form-horizontal" action="" role="form" method="POST" enctyle="multipart/form-data">
        <div class="form-group">
          <label for="nombre" class="col-sm-2 control-label">Nombre</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="nombre" placeholder="Nombre">
          </div>
        </div>
        <div class="form-group">
          <label for="apellido" class="col-sm-2 control-label">Apellido</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="apellido" placeholder="Apellido">
          </div>
        </div>
        <div class="form-group">
          <label for="correo" class="col-sm-2 control-label">Correo electrónico</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" name="correo" placeholder="Dirección de correo">
          </div>
        </div>
        <div class="form-group">
          <label for="Asunto" class="col-sm-2 control-label">Asunto</label>
          <div class="col-sm-10">
            <select id="asunto" name="asunto" class="form-control">
              <option value="Información usuarios">Información Usuarios</option>
              <option value="Información desarrolladores">Información Desarrolladores</option>
              <option value="Información administradores">Información Administradores</option>
              <option value="Galería">Galería</option>
              <option value="Bibliografía">Bibliografía</option>
              <option value="Otros">Otros</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="comentarios" class="col-sm-2 control-label">Comentario</label>
          <div class="col-sm-10">
            <textarea class="form-control" rows="3" name="comentarios" placeholder="Comentarios"></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-8">
            <div class="g-recaptcha" data-sitekey="6Lcawg4TAAAAAGBXE3AEbPF3bbiGQQeIFIaA2NZz"></div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-8">
            <button type="submit" class="btn btn-primary">Enviar</button>
            <button type="reset" class="btn btn-default">Borrar</button>
          </div>
        </div>
      </form>
<?php } ?>
    </section>

    <?php include '../../base/footer.php'; ?>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script>
      $('#asunto').select2({minimumResultsForSearch: Infinity});
    </script>
  </body>
</html>
