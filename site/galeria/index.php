<!DOCTYPE html>
<html lang="es">
  <head>
<?php
$contacto=false;
$galeria=true;
$titulo="Galería";
include '../../base/head.php';
?>
  </head>

  <body>
    <header>
<?php
$current="Galería";
include '../../base/navbar.php';
?>
    </header>

    <section class="container">
      <h1 class="page-header">
        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
        Galería
      </h1>
      <div class="row">
<?php
for ($i=1; $i<=6; $i++) {
  $imagen="../../static/img/galeria$i";
  $full="$imagen.jpg";
  $min="$imagen-min.jpg";
?>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
          <a class="thumbnail" href="<?= $full ?>" data-lightbox="galeria">
            <img class="img-responsive" src="<?= $min?>" alt="">
          </a>
        </div>
<?php }?>
      </div>
    </section>

    <?php include '../../base/footer.php'; ?>
    <script src="../../static/js/lightbox-plus-jquery.min.js"></script>
  </body>
</html>
