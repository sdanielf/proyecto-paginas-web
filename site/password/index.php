<!DOCTYPE html>
<html lang="es">
  <head>
<?php
$contacto=false;
$galeria=false;
$titulo="Verifica tu contraseña";
include '../../base/head.php';
?>
  </head>
  <body>
    <header>
<?php
$current="password";
include '../../base/navbar.php';
?>
    </header>

    <section class="container">
      <h1 class="page-header">
        <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
        Prueba la seguridad de tu contraseña
      </h1>
      <input id="password1" type="password" class="form-control" placeholder="Contraseña">
      <div class="progress">
        <div class="progress-bar" role="progressbar" style="width: 20%;">
          Muy mala
        </div>
      </div>
    </section>

    <?php include '../../base/footer.php'; ?>
    <script src="../../static/js/password.js"></script>
  </body>
</html>
