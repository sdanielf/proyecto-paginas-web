<nav class="navbar navbar-inverse navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Desplegar navegación</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button> <a class="navbar-brand" href="../home">Seguridad Informática</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li
          <?php if ($current == "index") {?>
             class="active"
          <?php } ?>
        >
          <a href="../home">
            Inicio
            <?php if ($current == "index") {?>
              <span class="sr-only">(actual)</span>
            <?php } ?>
          </a>
        </li>
        <li class="dropdown
                  <?php if ($current == "consejos") {?>active<?php }?>
          ">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
            aria-haspopup="true" aria-expanded="false">
            Consejos
            <?php if ($current == "consejos") {?>
              <span class="sr-only">(actual)</span>
            <?php } ?>
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="../usuarios">Usuarios</a></li>
            <li><a href="../desarrolladores">Desarrolladores y Administradores</a></li>
            <li><a href="../password">
              <b>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                Verificar tu contraseña
                <span class="label label-default">Nuevo</span>
              </b>
            </a></li>
          </ul>
        </li>
        <li
          <?php if ($current == "galeria") {?>
             class="active"
          <?php } ?>
        >
          <a href="../galeria">
            Galería
            <?php if ($current == "galeria") {?>
              <span class="sr-only">(actual)</span>
            <?php } ?>
          </a>
        </li>
        <li
          <?php if ($current == "bibliografia") {?>
             class="active"
          <?php } ?>
        >
          <a href="../bibliografia">
            Bibliografía
            <?php if ($current == "bibliografia") {?>
              <span class="sr-only">(actual)</span>
            <?php } ?>
          </a>
        </li>
        <li
          <?php if ($current == "bitacora") {?>
             class="active"
          <?php } ?>
        >
          <a href="../bitacora">
            Bitácora
            <?php if ($current == "bitacora") {?>
              <span class="sr-only">(actual)</span>
            <?php } ?>
          </a>
        </li>
        <li
          <?php if ($current == "contacto") {?>
             class="active"
          <?php } ?>
        >
          <a href="../contacto">
            Contacto
            <?php if ($current == "contacto") {?>
              <span class="sr-only">(actual)</span>
            <?php } ?>
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
