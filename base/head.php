<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="seguridad, IT, informatica">
<meta name="description" content="Seguridad informática para usuarios, administradores y desarrolladores">
<meta name="author" content="Lucas Di Landro, Daniel Francis, Gonzalo Gorostiza. ESI">

<title>
  <?php
    if ($titulo != "") {
      printf("%s -", $titulo);
    }
  ?>
  Seguridad Informática
</title>

<link rel="icon" href="../../static/img/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="../../static/img/favicon.ico" type="image/x-icon"/>

<link href="../../static/css/bootstrap.min.css" rel="stylesheet">
<?php if ($galeria) {?>
  <link href="../../static/css/thumbnail-gallery.css" rel="stylesheet">
  <link href="../../static/css/lightbox.css" rel="stylesheet">
<?php } ?>
<link href="../../static/css/colores.css" rel="stylesheet">
<link href="../../static/css/estilos.css" rel="stylesheet">

<?php if ($contacto) {?>
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<?php } ?>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
