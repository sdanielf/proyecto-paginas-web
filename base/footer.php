<footer>
  <p>
    Copyright
    <span class="glyphicon glyphicon-copyright-mark" aria-hidden="true"></span>
    2015, Escuela Superior de Informática Buceo
  </p>
  <p>
    <a href="mailto:lucasdilandro@gmail.com">Lucas Di Landro</a>,
    <a href="mailto:santiago.danielfrancis@gmail.com">Daniel Francis</a>,
    <a href="mailto:isebgonzalogorostiza@gmail.com">Gonzalo Gorostiza</a>
  </p>
</footer>

<script src="../../static/js/jquery.min.js"></script>
<script src="../../static/js/bootstrap.min.js"></script>
