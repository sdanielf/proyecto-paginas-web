(function () {
  var caracteres = ['.', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
                    'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
                    'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8',
                    '9'];

  $('#password1').keypress(function () {
    var value = $('#password1').val();
    var puntos = 0;
    puntos += value.length;
    for (var i=0; i < value.length; i++) {
      var caracter = value[i];
      if (caracteres.indexOf(caracter) > -1) {
        puntos += 2;
      }
    }
    var resultado = 1;
    var mensaje = 'Muy mala';
    if (puntos > 5) {
      resultado = 2;
      mensaje = 'Mala';
    }
    if (puntos > 10) {
      resultado = 3;
      mensaje = 'Buena';
    }
    if (puntos > 15) {
      resultado=4;
      mensaje='Muy buena';
    }
    if (puntos > 20) {
      resultado=5;
      mensaje='Excelente';
    }

    $('.progress-bar').css('width', '' + resultado * 20 + '%');
    $('.progress-bar').text(mensaje);
  });
})();
